\documentclass{beamer}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{tikz}

\tikzset{
  treenode/.style = {shape=rectangle, rounded corners,
                     draw, align=center,
                     color=black},
  root/.style     = {treenode, font=\Large, color=red!30},
  env/.style      = {treenode, font=\ttfamily\normalsize},
  dummy/.style    = {circle,draw}
}



% own commands
\newcommand{\distRaw}[2]{d(#1,#2)}
\newcommand{\dist}[2]{\ensuremath{\distRaw{\point{#1}}{\point{#2}}}}
\newcommand{\point}[1]{\ensuremath{\mathbf{#1}}}
\newcommand{\code}[1]{\texttt{#1}}


\title{Decision Procedures for Metric Spaces}
\subtitle{Bachelor's Thesis}

\author{Maximilian Schäffeler\\ Supervisor: Prof. Dr. Tobias Nipkow\\Advisor: Dr. Fabian Immler}

\institute[TU Munich]
{
  Chair for Logic and Verification\\
  Department of Informatics\\
  TU Munich
}

\date{17.10.2018}

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}



\section{Metric Spaces}
\begin{frame}
  \frametitle{Metric Spaces}
  \framesubtitle{A First-Order Theory}
  \begin{block}{Language of metric spaces $\mathcal{L}_M$}
    \begin{itemize}
    \item 2-sorted first-order language, sorts $\mathcal{V}$ and $\mathcal{R}$
    \item function $d :: \mathcal V \Rightarrow \mathcal V \Rightarrow \mathcal R$
      % +constants
    \item predicate on points: equality comparison $\mathbf v = \mathbf w$
      % +usual comparisons for scalars
    \end{itemize}
  \end{block}

  \begin{block}{Metric space axioms}
  \begin{itemize}
  \item \textit{positive definiteness}: $\dist{x}{y} \geq 0 \land (\dist{x}{y} = 0 \longleftrightarrow \point{x} = \point{y})$
  \item \textit{symmetry}: $\dist{x}{y} = \dist{y}{x}$
  \item \textit{triangle inequality}: $\dist{x}{z} \leq \dist{x}{y} + \dist{y}{z}$
  \end{itemize}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Decidability of our Theory for Metric Spaces}
  \begin{theorem}
    The fundamental theory of metric spaces is undecidable.
  \end{theorem}
  % proof might be too much
  \begin{proof}
    An arbitrary symmetric reflexive binary relation $\text{R}$ can be encoded as a metric via:
    $$\dist{x}{y} = \begin{cases}
      0 & \text{if } \point{x}=\point{y} \\
      1 & \text{if } \point{x}\neq\point{y} \land \text{R}(\point{x},\point{y}) \\
      2 & \text{if } \lnot\text{R}(\point{x},\point{y})
    \end{cases}$$
The theory of a symmetric reflexive binary relation is undecidable.
  \end{proof}
\end{frame}



\begin{frame}
  \frametitle{The $\forall\exists_p$ Fragment}
  $\forall\exists_p$ sentences are a superset of the set of $\forall\exists$ sentences.
  \vspace{1em}
  
  A sentence is $\forall\exists_p$, if
  \begin{itemize}
  \item it is in prenex normal form
  \item no universal quantifier over points is in the scope of any existential quantifier
  \end{itemize}
  \vspace{1em}

  \begin{example}
  $\forall\point{x}. \exists\point{y}. \forall r.~r \leq 0 \longrightarrow \dist{x}{y} \geq r$ is not $\forall\exists$, but is $\forall\exists_p$
  \end{example}
  \vspace{1em}

  \begin{theorem}
    The $\forall\exists_p$ fragment of the theory of metric spaces is decidable.
  \end{theorem}  
\end{frame}



\begin{frame}
  \frametitle{Decision Procedure for the $\forall\exists_p$ Fragment}
  \framesubtitle{Main Ideas}
  \textbf{Goal}: Provide a decision procedure for valid $\forall\exists_p$ sentences
  \begin{itemize}
  \item Consider an arbitrary $\forall\exists_p$ sentence $\phi$
  \item Eliminate all point existential quantifiers in $\phi$
  \item Embed $\phi$ into $\mathbb{R}^n$
  \item Apply a decision procedure for real arithmetic
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Decision Procedure for the $\forall\exists_p$ Fragment}
  
  \framesubtitle{Elimination of Point Existential Quantifiers}
  \begin{lemma}
  Assume $\phi \equiv \forall \point{x}_1 \ldots \point{x}_n. \exists \point{\overline y}/ \text{Q} \overline z.~ \psi$\\
Then $\phi$ is valid iff the result of replacing all subterms $\exists\point{y}.~\rho$ in $\phi$ with
$\rho[\point{x}_1/\point{y}] \lor \ldots \lor \rho[\point{x}_n/\point{y}]$ is valid.
\end{lemma}
\vspace{1em}
  Thus $\phi$ is equivalid to $\forall \point{x}_1 \ldots \point{x}_n.\ \psi'$ for some $\psi'$ that contains no quantifiers over points.
\end{frame}



\begin{frame}
  \frametitle{Decision Procedure for the $\forall\exists_p$ Fragment}
  \framesubtitle{Embedding into $(\mathbb{R}^n,d_\infty)$}
  \begin{theorem}[Isometric Embedding]
  Let $M$ be a finite metric space with $n$ members $\point{p}_1,\ldots,\point{p}_n$. \\
  Define $f_M: M \to \mathbb{R}^n$ by $f_M(\point{p}) = (\distRaw{\point p}{\point{p}_1}, \ldots , \distRaw{\point{p}}{\point{p}_n})$. \\
  Let $d_\infty$ be the metric induced by the supremum norm. \\
  Then $f_M$ is an isometric embedding of $M$ in $(\mathbb{R}^n,\textit{d}_\infty)$.
  \end{theorem}
\vspace{1em}

It follows that $\forall \point{x}_1\ldots\point{x}_n.~ \psi$ is valid iff it is valid in $(\mathbb{R}^n, d_\infty)$.\\
% \begin{itemize}
% \item point equality tests $\rightarrow$ component-wise equality
% \item point universal quantifiers $\rightarrow$ quantifiers over components
% \end{itemize}
Now we can apply a decision procedure for real arithmetic.
\end{frame}

\begin{frame}
  \frametitle{Implementation of the Decision Procedure}
  \framesubtitle{Requirements on Input Terms}
  \begin{block}{Limitations}
  \begin{itemize}
  \item no existential quantifiers over scalars allowed \\
    e.g. $\exists r. ~\dist{x}{y} \leq r$
  \item goal has to be LRA\\
    e.g. $\dist{x}{y} \cdot \dist{y}{z} \geq 0$ does not work
  \end{itemize}
\end{block}
\begin{block}{Features}
  \begin{itemize}
  \item subclasses of \texttt{metric\_space} possible\\
    e.g. $\dist{x+y}{z} \geq 0$
  \item open formulas can be handled
  \end{itemize}
\end{block}
\end{frame}

\begin{frame}
  \frametitle{Implementation of the Decision Procedure}
  \framesubtitle{Point Quantifier Elimination}
  \begin{example}
    $\bigwedge \point{a}~r~\point{x}~\point{y}.~\dist{x}{a} + \dist{a}{y} = r \Longrightarrow \\ \qquad\qquad\qquad\qquad\qquad\forall \point{z}.~ r \leq \dist{x}{z} + \dist{z}{y} \Longrightarrow \dist{x}{y} = r$
  \end{example}
  \begin{block}{Elimination of Universal Quantifiers}
    \begin{enumerate}
  \item Atomize goal
  \item Rewrite to prenex normal form
  \item Remove all outer universal quantifiers
  \end{enumerate}
\end{block}
\begin{example}
  $\exists \point{z}.\ \dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{z} + \dist{z}{y} < r \lor \dist{x}{y} = r$
\end{example}
\end{frame}

\begin{frame}
  \frametitle{Implementation of the Decision Procedure}
  \framesubtitle{Elimination of Point Existential Quantifiers}
  Current goal is of the form $\phi\equiv\exists\bar{\textbf{y}}/ \forall \bar{z}.~\rho$, $\rho$ quantifier-free
  \vspace{1em}
  
\begin{tikzpicture}
  [
    grow                    = right,
    sibling distance        = 10em,
    level distance          = 9em,
    edge from parent/.style = {draw, -latex},
    every node/.style       = {font=\footnotesize},
    sloped
  ]
  \node {elim\_exists}
    child { node {call elim\_exists on $\psi$}
      edge from parent node [below] {$\phi\equiv \forall z.~ \psi$}}
    child { node [dummy] {}
      child {
        node {continue with embedding}
        edge from parent node [below] {$\phi$ quantifier-free}}
      child {
        node {call elim\_exists on $\psi[\point{p}/\point{y}]$, $\point{p} \in \textit{points}$}
        edge from parent node [below] {$\phi\equiv \exists\point{y}. \psi$}
      }   
      edge from parent node [below] {otherwise}
    };
\end{tikzpicture}
\end{frame}


\begin{frame}
  \frametitle{Implementation of the Decision Procedure}
  \framesubtitle{Elimination of Point Existential Quantifiers - Example}
    \begin{example}
    \begin{block}{Before}
      $\exists \point{z}.\ \dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{z} + \dist{z}{y} < r \lor \dist{x}{y} = r$
    \end{block}

    \vspace{2em}
    $\textit{points} = \{\point{a},\point{y},\point{x}\}$
    \begin{block}{1st Choice: $\point{a}$ for $\point{z}$}
      $\dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{a} + \dist{a}{y} < r \lor \dist{x}{y} = r$\\
      logically equivalent to $\dist{x}{a} + \dist{a}{y} = r \longrightarrow \dist{x}{y} = r$
    \end{block}
    
    \begin{block}{2nd Choice: $\point{y}$ for $\point{z}$}
      $\dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{y} + \dist{y}{y} < r \lor \dist{x}{y} = r$
    \end{block}
    \end{example}
\end{frame}


\begin{frame}
  \frametitle{Implementation of the Decision Procedure}
  \framesubtitle{Embedding in $(\mathbb{R}^n, d_\infty)$}
  \begin{theorem}[maxdist\_thm]
    \vspace{-2em}
\begin{multline*}
\textit{finite}~\textit{points} \Longrightarrow \point{x} \in \textit{points} \Longrightarrow \point{y}\in\textit{points} \Longrightarrow \\ \dist{x}{y} = \bigsqcup ((\lambda~ \point{a}.~ |\dist{x}{a} - \dist{a}{y}|) ~ \grave{} ~ \textit{points})
\end{multline*}
\end{theorem}
\vspace{-2em}
  \begin{example}
  \begin{block}{Before}
    $\dist{a}{x} + \dist{a}{y} \neq r \lor \dist{x}{y} < r \lor \dist{x}{y} = r$
  \end{block}
\end{example}
  \begin{block}{After}
    $\max\{\dist{a}{x}, |\dist{a}{y} - \dist{x}{y}|\} + \\
    \hspace{8em}\max\{\dist{a}{y}, |\dist{a}{x} - \dist{x}{y}|\} \neq r \lor{}\\
    \hspace{8em}\max\{\dist{x}{y}, |\dist{a}{x} - \dist{a}{y}|\} < r \lor {} \\
    \hspace{8em}\max\{\dist{x}{y}, |\dist{a}{x} - \dist{a}{y}|\} = r$
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Case Study}
  \framesubtitle{Example from HOL Light}
  \begin{theorem}
    \vspace{-2em}
  \begin{multline*}
\lnot~\textit{disjnt}~(\textit{ball}~\point{x}~r) (\textit{ball}~\point{y}~s) \Longrightarrow \\(\forall \point{p}~\point{q}.~\point{p} \in (\textit{ball}~\point{x}~r \cup \textit{ball}~\point{y}~s) \land \point{q} \in (\textit{ball}~\point{x}~r \cup \textit{ball}~\point{y}~s) \longrightarrow \\ \dist{p}{q} < 2*(r+s))
\end{multline*}
\end{theorem}

\vspace{-1em}
\begin{tikzpicture}[scale=0.75, thick]
\begin{scope}
%\fill[lightgray] (0,0) circle (1.75cm);
\draw (0,0) circle (1.75cm);

%\fill[lightgray] (2,-0.5) circle (2cm);
\draw (2.5,-0.5) circle (2cm);

\draw [clip](0,0) circle (1.75cm);
\fill[lightgray] (2.5,-0.5) circle (2cm);
\end{scope}

\draw (0,0) node {$\point{x}$};
\draw (2.5,-0.5) node {$\point{y}$};

\draw (0,0) (-0.3, 0.8) node {r};
\draw [dotted] (0,0) -- (130:1.75cm);

\draw (0,0) (2.8, 0.45) node {s};
\draw [dotted] (2.5,-0.5) -- ++ (60:2cm);

\draw (-0.7,-1) node {$\point{p}$};
\draw (3.4,-1.5) node {$\point{q}$};

\draw [loosely dashed] (-0.5,-1) -- (3.1, -1.4);
\end{tikzpicture}

\begin{block}{Proof}
  Unfold the definitions of \textit{disjnt} and
  set union $\cup$, then apply \texttt{metric}
\end{block} 

\end{frame}

\section*{Summary}

\begin{frame}{Summary}
  \begin{itemize}
  \item
    \alert{Decision procedures} are a valuable tool in proof assistants.
  \item
    The method \texttt{metric} may prove statements in \alert{metric spaces} with reasonable running time
  \end{itemize}
  
  \begin{itemize}
  \item
    Outlook
    \begin{itemize}
    \item
      Further performance optimization
    \item
      Integration into the Isabelle distribution
    \end{itemize}
  \end{itemize}
\end{frame}

\end{document}